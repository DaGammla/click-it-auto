/*
 * Copyright DaGammla 2022
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE.txt or copy at
 *           https://www.boost.org/LICENSE_1_0.txt)
 */

package da.gammla;

import com.github.kwhat.jnativehook.GlobalScreen;

import javax.swing.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Program {
    public static void main(String[] args){

        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);

        try {
            GlobalScreen.registerNativeHook();

            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            Gui gui = new Gui();

        } catch (Exception ignored){

        }

    }
}
