/*
 * Copyright DaGammla 2022
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE.txt or copy at
 *           https://www.boost.org/LICENSE_1_0.txt)
 */

package da.gammla;

import com.github.kwhat.jnativehook.GlobalScreen;
import com.github.kwhat.jnativehook.keyboard.NativeKeyEvent;
import com.github.kwhat.jnativehook.keyboard.NativeKeyListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Gui extends JFrame implements NativeKeyListener {
    private JComboBox<String> timeCombo;
    private JSpinner speedSpinner;
    private JSpinner countSpinner;
    private JComboBox<String> mouseCombo;
    private JSlider portionSlider;
    private JButton changeKeyButton;
    private JCheckBox ctrlCheckBox;
    private JCheckBox altCheckBox;
    private JCheckBox shiftCheckBox;
    private JButton startButton;
    private JPanel basePanel;
    private JLabel currentKeyLabel;
    private JRadioButton holdRadioButton;
    private JRadioButton toggleRadioButton;

    private ClickThread clickThread;
    private final Robot robot;

    private boolean alwaysSave;
    private double downMillis;
    private double waitMillis;
    private int clickCount;
    private boolean active;
    private boolean clicking;
    private boolean changingKey;
    private int keyCode;
    private char keyChar;
    private boolean keyReleased;
    private boolean isActivationModeHold;
    private ClickTypeEnum clickType;

    public Gui() throws AWTException {
        super("Click it Auto");

        robot = new Robot();
        keyReleased = true;

        setContentPane(basePanel);
        pack();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        JFrame self = this;

        addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                stop();
                if (!alwaysSave) {
                    String[] options = new String[]{"Yes", "No", "Always"};
                    int response = JOptionPane.showOptionDialog(self, "Do you want so save your settings?", "Click it Auto",
                            JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE,
                            null, options, options[0]);

                    if (response == 0) {
                        saveSettings();
                    } else if (response == 2) {
                        alwaysSave = true;
                        saveSettings();
                    }
                } else {
                    saveSettings();
                }

                System.exit(1);
            }
        });

        speedSpinner.addChangeListener(e -> recalculate());
        countSpinner.addChangeListener(e -> recalculate());
        timeCombo.addItemListener(e -> recalculate());
        portionSlider.addChangeListener(e -> recalculate());

        holdRadioButton.addChangeListener(e -> applyActivationMode());
        toggleRadioButton.addChangeListener(e -> applyActivationMode());

        mouseCombo.addItemListener(e ->{
            String item = (String) mouseCombo.getSelectedItem();
            assert item != null;
            if (item.contains("Left")){
                clickType = ClickTypeEnum.LEFT_CLICK;
            } else if (item.contains("Right")){
                clickType = ClickTypeEnum.RIGHT_CLICK;
            } else if (item.contains("Middle")){
                clickType = ClickTypeEnum.MIDDLE_CLICK;
            }

            if (item.contains("Scroll")){
                if (item.contains("up")){
                    clickType = ClickTypeEnum.SCROLL_UP;
                } else {
                    clickType = ClickTypeEnum.SCROLL_DOWN;
                }
                portionSlider.setMaximum(0);
            } else if (portionSlider.getMaximum() == 0) {
                portionSlider.setMaximum(100);
                portionSlider.setValue(50);
            }
        });

        startButton.addActionListener(e -> {
            if (active){
                deactivate();
            } else {
                activate();
            }
        });

        changeKeyButton.addActionListener(e -> {
            if (changingKey){
                applyKey();
            } else {
                changeKeyButton.setText("Press any key");
                changeKeyButton.setBackground(Color.GREEN);
                changingKey = true;
            }
        });

        GlobalScreen.addNativeKeyListener(this);

        setLocationRelativeTo(null);
        setDefaultLookAndFeelDecorated(true);
        setResizable(true);
        setVisible(true);
        pack();

        keyCode = NativeKeyEvent.VC_C;
        keyChar = 'C';
        clickType = ClickTypeEnum.LEFT_CLICK;
        timeCombo.setSelectedIndex(2);

        readSettings();

        recalculate();
        applyKey();
        applyActivationMode();
    }

    private void createUIComponents() {
        SpinnerNumberModel model = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);
        speedSpinner = new JSpinner(model);
        model = new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1);
        countSpinner = new JSpinner(model);
    }

    private void recalculate(){
        String timeScale = (String) timeCombo.getSelectedItem();
        double timeMultiplier = 1;
        if ("hour".equals(timeScale)){
            timeMultiplier = 60 * 60 * 1000;
        } else if ("minute".equals(timeScale)){
            timeMultiplier = 60 * 1000;
        } else if ("second".equals(timeScale)){
            timeMultiplier = 1000;
        }

        double time = timeMultiplier / (int) speedSpinner.getValue();

        double portion = portionSlider.getValue() / 100.0;

        downMillis = time * portion;
        waitMillis = time - downMillis;

        clickCount = (int) countSpinner.getValue();
    }

    private void applyActivationMode(){
        isActivationModeHold = holdRadioButton.isSelected();
    }

    private void activate(){
        active = true;

        startButton.setText("Stop");
        setConfigButtonsEnabled(false);
    }

    private void deactivate(){
        active = false;

        startButton.setText("Start");
        setConfigButtonsEnabled(true);
    }

    private void setConfigButtonsEnabled(boolean enabled){
        portionSlider.setEnabled(enabled);
        timeCombo.setEnabled(enabled);
        countSpinner.setEnabled(enabled);
        speedSpinner.setEnabled(enabled);
        mouseCombo.setEnabled(enabled);
    }

    private void start(){
        if (clickThread != null)
            clickThread.stopClick();

        clickThread = new ClickThread();
        clickThread.start();

        clicking = true;
        keyReleased = true;

        setControlButtonsEnabled(false);
    }

    private void stop(){
        if (clickThread != null)
            clickThread.stopClick();

        clicking = false;
        keyReleased = true;

        setControlButtonsEnabled(true);
    }

    private void threadFinished(){
        clicking = false;
        setControlButtonsEnabled(true);
    }

    private void setControlButtonsEnabled(boolean enabled){
        changeKeyButton.setEnabled(enabled);
        altCheckBox.setEnabled(enabled);
        shiftCheckBox.setEnabled(enabled);
        ctrlCheckBox.setEnabled(enabled);
        holdRadioButton.setEnabled(enabled);
        toggleRadioButton.setEnabled(enabled);
    }

    private void applyKey(){

        String keyDisplay;

        if (Character.isISOControl(keyChar) || Character.isWhitespace(keyChar) || !Character.isDefined(keyChar)){
            keyDisplay = NativeKeyEvent.getKeyText(keyCode);
        } else {
            keyDisplay = keyChar + "";
        }

        currentKeyLabel.setText("Current key: " + keyDisplay);

        changingKey = false;

        changeKeyButton.setBackground(null);
        changeKeyButton.setText("Change key");
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeEvent) {
        if (changingKey || !active)
            return;
        if (nativeEvent.getKeyCode() == keyCode && checkForModifiers(nativeEvent)){

            if (isActivationModeHold){
                if (!clicking && keyReleased)
                    start();
            } else if (keyReleased) {
                if (clicking){
                    stop();
                } else {
                    start();
                }
            }
            keyReleased = false;
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeEvent) {
        if (changingKey){
            keyCode = nativeEvent.getKeyCode();
            keyChar = nativeEvent.getKeyChar();
            applyKey();
        } else if (active) {
            if (nativeEvent.getKeyCode() == keyCode || !checkForModifiers(nativeEvent)){
                if (nativeEvent.getKeyCode() == keyCode) {
                    keyReleased = true;
                }

                if (isActivationModeHold && clicking){
                    stop();
                }
            }
        }
    }

    private boolean checkForModifiers(NativeKeyEvent event){
        int modifiers = event.getModifiers();
        if (ctrlCheckBox.isSelected() && (modifiers & NativeKeyEvent.CTRL_MASK) == 0){
            return false;
        }
        if (shiftCheckBox.isSelected() && (modifiers & NativeKeyEvent.SHIFT_MASK) == 0){
            return false;
        }
        return !altCheckBox.isSelected() || (modifiers & NativeKeyEvent.ALT_MASK) != 0;
    }

    private void readSettings(){
        String dir = System.getProperty("user.dir");
        File settingsFile = Paths.get(dir, "clickItAuto.conf").toFile();

        HashMap<String, String> settings = new HashMap<>();

        if (settingsFile.exists()) try {
            BufferedReader br = new BufferedReader(new FileReader(settingsFile));
            String line;
            while ((line = br.readLine()) != null) {
                String[] split = line.split("=");
                if (split.length == 2){
                    settings.put(split[0].trim(), split[1].trim());
                }
            }
            br.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        if (settings.containsKey("always_save")){
            alwaysSave = settings.get("always_save").equals("true");
        }

        if (settings.containsKey("click_portion")){
            portionSlider.setValue(Integer.parseInt(settings.get("click_portion")));
        }

        if (settings.containsKey("click_speed")){
            speedSpinner.setValue(Integer.parseInt(settings.get("click_speed")));
        }

        if (settings.containsKey("per_time")){
            timeCombo.setSelectedItem(settings.get("per_time"));
        }

        if (settings.containsKey("click_count")){
            countSpinner.setValue(Integer.parseInt(settings.get("click_count")));
        }

        if (settings.containsKey("click_type")){
            mouseCombo.setSelectedItem(settings.get("click_type"));
        }

        if (settings.containsKey("key_code") && settings.containsKey("key_char")){
            keyCode = Integer.parseInt(settings.get("key_code"));
            keyChar = (char) Integer.parseInt(settings.get("key_char"));
        }

        if (settings.containsKey("activation_mode_hold")){
            boolean hold = settings.get("activation_mode_hold").equals("true");
            holdRadioButton.setSelected(hold);
            toggleRadioButton.setSelected(!hold);
        }

        if (settings.containsKey("plus_alt")){
            altCheckBox.setSelected(settings.get("plus_alt").equals("true"));
        }

        if (settings.containsKey("plus_shift")){
            shiftCheckBox.setSelected(settings.get("plus_shift").equals("true"));
        }

        if (settings.containsKey("plus_ctrl")){
            ctrlCheckBox.setSelected(settings.get("plus_ctrl").equals("true"));
        }
    }

    private void saveSettings(){
        String dir = System.getProperty("user.dir");
        File settingsFile = Paths.get(dir, "clickItAuto.conf").toFile();

        if (settingsFile.exists()) {
            settingsFile.delete();
        }
        try {
            String ls = System.lineSeparator();
            BufferedWriter bw = new BufferedWriter(new FileWriter(settingsFile));

            bw.write("always_save=" + alwaysSave);
            bw.write(ls);

            bw.write("click_portion=" + portionSlider.getValue());
            bw.write(ls);

            bw.write("click_speed=" + speedSpinner.getValue());
            bw.write(ls);

            bw.write("per_time=" + timeCombo.getSelectedItem());
            bw.write(ls);

            bw.write("click_count=" + countSpinner.getValue());
            bw.write(ls);

            bw.write("click_type=" + mouseCombo.getSelectedItem());
            bw.write(ls);

            bw.write("key_code=" + keyCode);
            bw.write(ls);

            bw.write("key_char=" + (int) keyChar);
            bw.write(ls);

            bw.write("activation_mode_hold=" + isActivationModeHold);
            bw.write(ls);

            bw.write("plus_alt=" + altCheckBox.isSelected());
            bw.write(ls);

            bw.write("plus_shift=" + shiftCheckBox.isSelected());
            bw.write(ls);

            bw.write("plus_ctrl=" + ctrlCheckBox.isSelected());
            bw.write(ls);

            bw.close();

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private class ClickThread extends Thread {
        private boolean running = true;

        private boolean mouseDown = false;
        ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

        @Override
        public void run() {
            running = true;
            mouseDown = false;

            lock.readLock().lock();

            int clicksPerformed = 0;

            while (running && (clickCount == 0 || clicksPerformed < clickCount)) {

                press();

                lock.readLock().unlock();
                doubSleep(downMillis);


                lock.readLock().lock();
                if (!running)
                    break;

                release();

                lock.readLock().unlock();
                doubSleep(waitMillis);

                clicksPerformed++;

                lock.readLock().lock();
            }

            lock.readLock().unlock();

            threadFinished();
        }

        private void press(){
            mouseDown = true;
            switch (clickType){
                case LEFT_CLICK:
                    robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                    break;
                case RIGHT_CLICK:
                    robot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
                    break;
                case MIDDLE_CLICK:
                    robot.mousePress(InputEvent.BUTTON2_DOWN_MASK);
                    break;
                case SCROLL_UP:
                    robot.mouseWheel(-1);
                    break;
                case SCROLL_DOWN:
                    robot.mouseWheel(1);
                    break;
            }

        }

        private void release(){
            mouseDown = false;
            switch (clickType){
                case LEFT_CLICK:
                    robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                    break;
                case RIGHT_CLICK:
                    robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
                    break;
                case MIDDLE_CLICK:
                    robot.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);
                    break;
            }
        }

        public void stopClick(){
            lock.writeLock().lock();
            running = false;
            if (mouseDown)
                release();
            lock.writeLock().unlock();
        }

        private void doubSleep(double millis){
            try {
                Thread.sleep((long)millis, (int) ((millis - (long) millis) * 1000000));
            } catch (Exception ignored){ }
        }
    }
}
